var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/:n1/:n2', function(req, res, next) {
  const n1 = parseInt( req.params.n1);
  const n2 = parseInt( req.params.n2);
  res.send( `${n1 + n2}`);
});

module.exports = router;
